/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.example.dailyhealthyeatingtips.R;

public class splash extends Activity {

	 //protected boolean active = true;
	 //protected int splashTime = 1000;
	 
	 @Override
	 public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.splash);

	        int secondsDelayed = 1;
	        new Handler().postDelayed(new Runnable() {
	                public void run() {
	                	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
	                	if(preferences.getBoolean("pref_key_data_setted", false)){
	                			startActivity(new Intent(splash.this, Launcher.class));
		                    	finish();
	                	}else{
	                		if(	preferences.getString("pref_key_user", "User").equals("User") ||
			                	preferences.getString("pref_key_email", "example@domain.com").equals("example@domain.com") ||
		                		preferences.getString("pref_key_age", "0").equals("0") ||
		                		preferences.getString("pref_key_matricula", "0").equals("0") ||
	                			preferences.getString("pref_key_height", "0").equals("0") ||
	                			preferences.getString("pref_key_weight", "0").equals("0")){
	                			startActivity(new Intent(splash.this, SettingsActivity.class));
	                    		finish();
	                		}else{
	                			Editor editor = preferences.edit();
	                			editor.putBoolean("pref_key_data_setted", true);
	                			editor.commit();
	                			startActivity(new Intent(splash.this, Launcher.class));
		                    	finish();
	                		}
	                	}
	                }
	        }, secondsDelayed * 2000);
	 }
}

