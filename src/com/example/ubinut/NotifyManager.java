/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

public class NotifyManager {
	 
public void playNotification(Context context, Class<?> cls, String textNotification, String titleNotification, int drawable){
  
	int FM_NOTIFICATION_ID = 1;
    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	long[] pattern={100,100,200,300};
	// Build notification
    NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
        .setContentTitle(titleNotification)
        .setContentText(textNotification)
        .setSmallIcon(drawable)
        .setVibrate(pattern)
        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        .setAutoCancel(true);
	
	// Prepare intent which is triggered if the
    // notification is selected
	Intent intent = new Intent(context, cls);
	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    PendingIntent pIntent =  PendingIntent.getActivity(context, 0, intent, android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
    builder.setContentIntent(pIntent);
      
    manager.notify(FM_NOTIFICATION_ID, builder.build());
	
	}
 
}
