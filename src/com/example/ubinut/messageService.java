/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.dailyhealthyeatingtips.R;

public class messageService extends Service {
	 private Timer mTimer = null; 
	 private static message mensaje = new message();
	 @Override
	 public IBinder onBind(Intent arg0) {
	  return null;
	 }
	 
	 @Override
	 public void onCreate(){
	  super.onCreate();
	  this.mTimer = new Timer();
	  this.mTimer.scheduleAtFixedRate(
	    new TimerTask(){
	     @Override
	     public void run() {
	      ejecutarTarea();
	     }      
	    }
	    , 0, 36000000);//tiempo en milisegundos de la frecuencia de actualización
	 }
	 
	 private void ejecutarTarea(){
		 
	  Thread t = new Thread(new Runnable() {
	   public void run() { 
		   	boolean update = false;
			try {
				update = mensaje.updateMessage(getApplicationContext());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(update){
				//se crea un objeto para manipular las notificaciones
				 NotifyManager notify = new NotifyManager();
				 //Se muestra una notificación al usuario de cambios en
				 //en el mensaje entregado por el servidor.
				 //si es seleccionada inicia la actividad splash
				    notify.playNotification(getApplicationContext(),
				      splash.class, getString(R.string.notif_text)
				      , getString(R.string.notif_title), R.drawable.notif);
			}
	   }
	  });  
	  t.start();
	 }
}
