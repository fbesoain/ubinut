/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dailyhealthyeatingtips.R;

public class Launcher extends Activity {
	
	private TabHost tabs;
	private Spinner spinner;
	private RatingBar ratingBar;
	dbHandler db;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_launcher);
		
		Resources res = getResources();
				 
		tabs=(TabHost)findViewById(android.R.id.tabhost);
		tabs.setup();
		 
		TabHost.TabSpec spec=tabs.newTabSpec("tab1");
		spec.setContent(R.id.tab1);
		spec.setIndicator("",
		    res.getDrawable(android.R.drawable.ic_menu_info_details));
		tabs.addTab(spec);
		 
		spec=tabs.newTabSpec("tab2");
		spec.setContent(R.id.tab2);
		spec.setIndicator("",
		    res.getDrawable(android.R.drawable.ic_menu_help));
		tabs.addTab(spec);
		
		spinner = (Spinner) findViewById(R.id.Options_spinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.Options_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
		
		ratingBar = (RatingBar) findViewById(R.id.puntuacionMsg);
		
		db = new dbHandler(this);
		if(savedInstanceState == null){
			TextView text = (TextView) findViewById(R.id.messageBody);
			text.setText(db.selectBody());
			tabs.setCurrentTab(0);
			Intent service = new Intent(this, messageService.class);
			startService(service);
		}else{
			tabs.setCurrentTab(savedInstanceState.getInt("LastTab"));
			TextView text = (TextView) findViewById(R.id.messageBody);
			text.setText(db.selectBody());
		}
		float puntaje=db.isRated();
		if(puntaje==6){
			//ratingBar Listener
			addListenerOnRatingBar();
		}else{
			ratingBar.setRating(puntaje);
			ratingBar.setEnabled(false);
		}
	}

	@Override
	protected void onSaveInstanceState (Bundle outState){
		super.onSaveInstanceState(outState);
	    outState.putInt("LastTab", tabs.getCurrentTab());
	}
	
	@Override
	public void onResume(){
		super.onResume();
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		int text_size = Integer.parseInt(sharedPref.getString(SettingsActivity.PREF_KEY_SIZE, "40"));
		TextView text = (TextView) findViewById(R.id.messageBody);
		text.setTextSize(text_size);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_launcher, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_settings:
            	Intent intent = new Intent(this, SettingsActivity.class);
            	startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
	
	/** Called when the user touches the button */
	public void sendQuestion(View view)  {
	    // Do something in response to button click
		EditText question = (EditText) findViewById(R.id.message);
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		float height = (float) Integer.parseInt(preferences.getString("pref_key_height", null));
		float weight = (float) Integer.parseInt(preferences.getString("pref_key_weight", null));
		int age = Integer.parseInt(preferences.getString("pref_key_age", null));
		
		json coneccion = new json();
		try {
			JSONArray jsonArray = new JSONArray(coneccion.sendQuestion(preferences.getString("pref_key_email", null), question.getText().toString() ,spinner.getSelectedItem().toString(), age, height, weight));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_LONG;
		Toast toast = Toast.makeText(context,R.string.sending_q, duration);
		toast.show();
	}
	
	public void addListenerOnRatingBar(){
		
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			public void onRatingChanged(RatingBar ratingBar, float rating,
				boolean fromUser) {
				json coneccion = new json();
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
				float height = (float) Integer.parseInt(preferences.getString("pref_key_height", null));
				float weight = (float) Integer.parseInt(preferences.getString("pref_key_weight", null));
				int age = Integer.parseInt(preferences.getString("pref_key_age", null));
				
				if(ratingBar.isEnabled()){
					try {
						JSONArray jsonArray = new JSONArray(coneccion.sendScore(preferences.getString("pref_key_email", null), rating, height, weight, db.selectDate(), age));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					int duration = Toast.LENGTH_SHORT;
					Context context = getApplicationContext();
					Toast toast = Toast.makeText(context, R.string.sent+" "+String.valueOf(rating), duration);
					db.setRated(rating);
					ratingBar.setEnabled(false);
					toast.show();
				}	 
			}
		});
	}
}
