/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

public class message{
	
	private String body;
	private String date;
	private String type;
	
	public String getbody() {
		return this.body;
	}
	public String getdate() {
		return this.date;
	}
	public String gettype() {
		return this.type;
	}
	
	public boolean updateMessage(Context contex) throws JSONException{
		dbHandler db = new dbHandler(contex);
		json coneccion = new json();
		JSONArray jsonArray = new JSONArray(coneccion.readJSON());
		if(db.selectDate().equals(jsonArray.getJSONObject(0).getString("id_message"))){
			return false;
		}else{
			this.body = jsonArray.getJSONObject(0).getString("message");
			this.date = jsonArray.getJSONObject(0).getString("id_message");
			this.type = jsonArray.getJSONObject(0).getString("type");
			db.updateMessage(this.date, this.type, this.body);
			return true;
		}
	}
}
