/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import com.example.dailyhealthyeatingtips.R;

public class SettingsActivity extends PreferenceActivity{
	private static int prefs=R.xml.preferences;
	public static final String PREF_KEY_SIZE = "pref_key_size";
	public static final String PREF_KEY_USER = "pref_key_user";

    @Override
    protected void onCreate(final Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        try {
            getClass().getMethod("getFragmentManager");
            AddResourceApi11AndGreater();
        } catch (NoSuchMethodException e) { //Api < 11
            AddResourceApiLessThan11();
        }
    }

    @SuppressWarnings("deprecation")
    protected void AddResourceApiLessThan11(){
        addPreferencesFromResource(prefs);
    }

    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	protected void AddResourceApi11AndGreater(){
        getFragmentManager().beginTransaction()
        .replace(android.R.id.content, new PF()).commit();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class PF extends PreferenceFragment{       
        @Override
        public void onCreate(final Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(SettingsActivity.prefs); //outer class private members seem to be visible for inner class, and making it static made things so much easier
        }
    }
}
