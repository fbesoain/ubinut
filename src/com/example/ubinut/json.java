/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

public class json{
	@SuppressLint("NewApi")
	public json(){
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		
	}

	public String readJSON() {
		HttpGet httpGet = null;
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		
		httpGet = new 
		HttpGet("http://srvbioinf.utalca.cl/nutritionFacts/actions/messageService.php?id=1");
		
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(json.class.toString(), "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	public String sendQuestion(String email, String question, String type, int edad, float altura, float peso) {

		 HttpGet httpGet = null;
         StringBuilder builder = new StringBuilder();
         HttpClient client = new DefaultHttpClient();
         email = email.replaceAll(" ", "_");
         question = question.replaceAll(" ", "_");
         httpGet = new
         HttpGet("http://srvbioinf.utalca.cl/nutritionFacts/actions/insertQuestion.php?email="+(email)+"&question="+(question)+"&type="+(type)+"&height="+altura+"&weight="+peso+"&age="+edad);
         
         try {
                 HttpResponse response = client.execute(httpGet);
                 StatusLine statusLine = response.getStatusLine();
                 int statusCode = statusLine.getStatusCode();
                 if (statusCode == 200) {
                         HttpEntity entity = response.getEntity();
                         InputStream content = entity.getContent();
                         BufferedReader reader = new BufferedReader(
                                         new InputStreamReader(content));
                         String line;
                         while ((line = reader.readLine()) != null) {
                                 builder.append(line);
                         }
                 } else {
                         Log.e(json.class.toString(), "Failed to download file");
                 }
         } catch (ClientProtocolException e) {
                 e.printStackTrace();
         } catch (IOException e) {
                 e.printStackTrace();
         }
         return builder.toString();
	}
	
	public String sendScore(String correo, float puntaje, float altura, float peso, String mess_id, int edad) {

		HttpGet httpGet = null;
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
	
        mess_id = mess_id.replaceAll(" ", "_");
        httpGet = new
        HttpGet("http://srvbioinf.utalca.cl/nutritionFacts/actions/insertScoring.php?score="+puntaje+"&id_message="+mess_id+"&email="+correo+"&height="+altura+"&weight="+peso+"&age="+edad);

		Log.e("XXXXXXXXXXXXXX","http://srvbioinf.utalca.cl/nutritionFacts/actions/insertScoring.php?score="+puntaje+"&id_message="+mess_id+"&email="+correo+"&height="+altura+"&weight="+peso+"&age="+edad);
        try {
                HttpResponse response = client.execute(httpGet);
                StatusLine statusLine = response.getStatusLine();
                int statusCode = statusLine.getStatusCode();
                if (statusCode == 200) {
                        HttpEntity entity = response.getEntity();
                        InputStream content = entity.getContent();
                        BufferedReader reader = new BufferedReader(
                                        new InputStreamReader(content));
                        String line;
                        while ((line = reader.readLine()) != null) {
                                builder.append(line);
                        }
                } else {
                        Log.e(json.class.toString(), "Failed to download file");
                }
        } catch (ClientProtocolException e) {
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }
        return builder.toString();
	}
}
