/*
    This file is part of Ubinut.

    Ubinut is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ubinut is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
    
    Authors: Felipe Ojeda A. and Felipe Beosain P.*/

package com.example.ubinut;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class dbHandler extends  SQLiteOpenHelper{

	//Constructor base de datos
	private static String name = "bdNitriFacts";
	private static int version = 1;
	private static CursorFactory cursorFactory = null;
	
	//Tablas base de datos
	protected static String tableMessage = "message";
	private String SQLCreateMessages = "CREATE TABLE " + tableMessage +  " (id_message DATE, type VARCHAR(45), message VARCHAR(200), rated REAL ) ";
	
	public dbHandler(Context context){
		 super(context,name,cursorFactory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQLCreateMessages);
	}

	public void updateMessage(String id,String type,String message){
		SQLiteDatabase db = getWritableDatabase();
		 if(db!=null){
			 Cursor queryResponse = db.rawQuery("SELECT count(*) FROM "+tableMessage,null);
			 queryResponse.moveToFirst();
			 if(queryResponse.getInt(0)>0){
				 db.execSQL("UPDATE " + tableMessage +" SET id_message='"+id+"',type='"+type+"',message='"+message+"',rated='6'");
				 db.close();
			 }else{
				 db.execSQL("INSERT INTO " + tableMessage +"(id_message,type,message,rated) VALUES('"+id+"','"+type+"','"+message+"','6')");
				 db.close();
			 }
		 }
	}
	
	public String selectDate(){
		String date = "";
		SQLiteDatabase db = getReadableDatabase();
		Cursor queryResponse = db.rawQuery("SELECT id_message FROM "+tableMessage,null);
		if(queryResponse.moveToFirst()){
		      do{
		    	  date = queryResponse.getString(0);  
		      }while(queryResponse.moveToNext());
		     }
		db.close();
		return date;
	}

	public String selectBody(){
		String body = "";
		SQLiteDatabase db = getReadableDatabase();
		Cursor queryResponse = db.rawQuery("SELECT message FROM "+tableMessage,null);
		if(queryResponse.moveToFirst()){
		      do{
		    	  body = queryResponse.getString(0);  
		      }while(queryResponse.moveToNext());
		     }
		db.close();
		return body;
	}
	
	public float isRated(){
		float ratedStatus = 6;
		SQLiteDatabase db = getReadableDatabase();
		Cursor queryResponse = db.rawQuery("SELECT rated FROM "+tableMessage,null);
		if(queryResponse.moveToFirst()){
			do{
				ratedStatus = queryResponse.getFloat(0);
			}while(queryResponse.moveToNext());
		}
		db.close();
		return ratedStatus;
	}
	
	public void setRated(float r){
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("UPDATE " + tableMessage +" SET rated='"+r+"'");
		db.close();
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
}

